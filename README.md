# Mobile Login

![License](https://img.shields.io/badge/license-MIT-open-group-blue.svg) [![Latest Stable Version](https://img.shields.io/packagist/v/wanecho/mobilelogin.svg)](https://packagist.org/packages/wanecho/mobilelogin) [![Total Downloads](https://img.shields.io/packagist/dt/wanecho/mobilelogin.svg)](https://packagist.org/packages/wanecho/mobilelogin)

A [Flarum](http://flarum.org) extension. mobile register login

## Installation
## Flarum 手机号登录插件,
- 提醒用户绑定手机号
- 设置界面用户自行绑定手机号 ,可修改绑定手机
- 手机号注册
- 手机号密码找回
- 后台用户列表可查看手机号
- 后台新增用户时,可填写手机号

Install with composer:

```sh
composer require wanecho/mobilelogin:"*"
```

## Updating

```sh
composer update wanecho/mobilelogin:"*"
php flarum migrate
php flarum cache:clear
```

## Links

- [Packagist](https://packagist.org/packages/wanecho/mobilelogin)
- [GitHub](https://gitee.com/echo886/flarum-mobilelogin)
- [Discuss](https://discuss.flarum.org/d/PUT_DISCUSS_SLUG_HERE)
