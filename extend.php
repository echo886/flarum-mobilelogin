<?php

/*
 * This file is part of wanecho/mobilelogin.
 *
 * Copyright (c) 2022 229643466@qq.com.
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Wanecho\Mobilelogin;

use Flarum\Extend;
use Wanecho\Mobilelogin\Api\GlobalPolicy;
use Flarum\Api\Serializer\UserSerializer;
use Flarum\Discussion\Discussion;
use Wanecho\Mobilelogin\Api\PostPolicy;
use Wanecho\Mobilelogin\Api\Validators\MobileValidator;
use Flarum\User\Event\Registered;
use Flarum\User\Event\Saving;

return [
    (new Extend\Frontend('admin'))
        ->js(__DIR__ . '/js/dist/admin.js'),

    (new Extend\Frontend('forum'))
        ->js(__DIR__ . '/js/dist/forum.js')
        ->css(__DIR__ . '/less/forum.less'),

    // new Extend\Locales(__DIR__ . '/locale'),
    //修改手机号
    (new Extend\Routes('api'))->post('/sendcode', 'wanecho-sendcode', Api\Controllers\SendCodeController::class),
    (new Extend\Routes('api'))->post('/smscheck', 'wanecho-smscheck', Api\Controllers\SmsCheckController::class),
    (new Extend\Routes('api'))->post('/editmobile', 'wanecho-editmobile', Api\Controllers\EditMobileController::class),
    //修改密码
    (new Extend\Routes('api'))->post('/sendchangecode', 'wanecho-changecode', Api\Controllers\SendChangeCode::class),
    (new Extend\Routes('api'))->post('/changePwd', 'wanecho-changePwd', Api\Controllers\ChangePwdController::class),
    //显示验证码
    (new Extend\Routes('forum'))->get('/captcha', 'wanecho-Captcha', Api\Controllers\CaptchaController::class),
    //前端手机号显示
    (new Extend\ApiSerializer(UserSerializer::class))->attribute('mobile', function ($serializer, $user, $attributes) {
        return $user->mobile;
    }),
    //权限
    (new Extend\Policy())->modelPolicy(Discussion::class, PostPolicy::class)->globalPolicy(GlobalPolicy::class),
    //手机号注册
    (new Extend\Event())->listen(Registered::class, function (Registered $event) {
        $event->user->activate();
        $event->user->save();
    })->listen(Saving::class, Api\Listeners\SavingValidate::class),
    //注册发送SMS
    (new Extend\Routes('api'))->post('/regsendcode', 'wanecho-regsendcode', Api\Controllers\RegSendcodeController::class),
    //手机号验证
    (new Extend\Validator(MobileValidator::class))->configure(Api\Listeners\AddValidatorRule::class),
    //找回密码
    (new Extend\Routes('api'))->post('/forgetpwd', 'wanecho-forget', Api\Controllers\ForgetpwdController::class),
    (new Extend\Routes('api'))->post('/forgetsendcode', 'wanecho-forgetsendcode', Api\Controllers\ForgetSendcodeController::class),
];
