import { extend } from 'flarum/common/extend';
import UserListPage from 'flarum/admin/components/UserListPage';
import CreateUserModal from 'flarum/admin/components/CreateUserModal';
import Stream from "flarum/common/utils/Stream";
export default function () {
    extend(UserListPage.prototype, 'columns', (columns) => {
        columns.add('mobile', {
            name: "手机号",
            content: (user) => {
                if (user.data.attributes.mobile) {
                    return <div>{user.data.attributes.mobile}</div>;
                } else {
                    return <div></div>;
                }
            },
        });
    });
    extend(CreateUserModal.prototype, 'oninit', function () {
        this.mobile = Stream('');
    });
    extend(CreateUserModal.prototype, 'fields', function (items) {
        items.add(
            'mobile',
            m(".Form-group", [
                m("input.FormControl", {
                    name: "mobile",
                    type: "text",
                    placeholder: 'mobile',
                    bidi: this.mobile
                }),
            ]),
            100
        );
    });
    extend(CreateUserModal.prototype, "submitData", function (data) {
        data.mobile = this.mobile;
        return data;
    });
}