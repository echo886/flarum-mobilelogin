import app from 'flarum/admin/app';
import UserListPage from './components/UserList'
app.initializers.add('wanecho/mobilelogin', () => {
  app.extensionData
    .for('wanecho-mobilelogin')
    .registerSetting({
      setting: 'mobilelogin.accessKeyId', // This is the key the settings will be saved under in the settings table in the database.
      label: 'accessKeyId', // The label to be shown letting the admin know what the setting does.
      type: 'text', // What type of setting this is, valid options are: boolean, text (or any other <input> tag type), and select. 
    })
    .registerSetting(
      {
        setting: 'mobilelogin.accessKeySecret',
        label: 'accessKeySecret',
        type: 'text',
      })
    .registerSetting({
      setting: 'mobilelogin.SignName',
      label: 'SignName（签名名称）',
      type: 'text',
    })
    .registerSetting({
      setting: 'mobilelogin.TemplateCode',
      label: 'TemplateCode（短信模板CODE）',
      type: 'text',
    });
  UserListPage();
});