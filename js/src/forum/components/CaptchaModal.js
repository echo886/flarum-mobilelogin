import app from 'flarum/forum/app';
import Modal from "flarum/common/components/Modal";
import ItemList from "flarum/common/utils/ItemList";
import Stream from 'flarum/utils/Stream';
import ForgetFormModal from './ForgetFormModal'
export default class CaptchaModal extends Modal {

  static isDismissibleViaBackdropClick = false;
  oninit(vnode) {
    super.oninit(vnode);
    this.Forget = this.attrs.Forget || '';
    this.mobile = Stream(this.attrs.mobile || '');
    this.captcha = Stream("");
    this.captcha_url = '/captcha'
  }
  className() {
    // Custom CSS classes to apply to the modal
    return 'captcha-modal-class Modal--small';
  }
  title() {
    return <p>图形验证码</p>;
  }
  content() {
    return (
      <div className="Modal-body">
        {this.form().toArray()}
      </div>
    );
  }

  form() {
    const items = new ItemList();
    items.add(
      'captcha',
      m(".Form-group", m('.auth-line', [
        m("input.FormControl.msgcode", {
          name: "captcha",
          type: "text",
          placeholder: '图形验证码',
          bidi: this.captcha,
          oninput: () => {
            console.log(3343);
          }
        }),
        m("img.captcha_img", {
          src: this.captcha_url,
          width: "120",
          onclick: () => {
            this.captcha_url = '/captcha?' + Math.random()
            m.redraw();
          }
        })
      ])),
      30
    )
    return items;
  }

  CheckCode() {
    this.issendcode = true
    app.request({
      method: 'POST',
      url: app.forum.attribute('apiUrl') + '/regsendcode',
      body: {
        mobile: this.mobile(),
        captcha: this.captcha(),
      },
    })
      .then(() => {
        app.alerts.show({ type: 'success' }, "短信发送成功");
        this.msgcode_text = "已发送"
        m.redraw();
      })
      .catch(() => {

        this.issendcode = false
      })
      .then(this.loaded.bind(this));
  }
}
