import app from 'flarum/forum/app';
import Modal from "flarum/common/components/Modal";
import ItemList from "flarum/common/utils/ItemList";
import Button from 'flarum/common/components/Button';
import Stream from 'flarum/utils/Stream';

export default class EditMobileModal extends Modal{
  static isDismissible = true;
  oncreate(vnode) {
    super.oncreate(vnode);
    this.mobile = this.geTel(app.session.user.mobile())|| '';
    this.count= 60;
    this.newmobile = Stream(this.attrs.newmobile || '');
    this.oldmobile = Stream(this.attrs.oldmobile || '');
    this.code = Stream(this.attrs.code || '');
    this.sendingSms = false;
    this.liked = true;
    this.loading = false;
  }
  className() {
    // Custom CSS classes to apply to the modal
    return 'editmobile-modal-class';
  }
  title() {
    return <p>修改手机号</p>;
  }
  content() {
      return (
        <div className="Modal-body">
          {this.form().toArray()}
        </div>
      );
  }
  form(){
    const items = new ItemList();
    items.add(
        'mobile',
        <div className="Form-group">
          <label>手机号</label>
          <div>{this.mobile}</div>
        </div>,
        60
    )
    items.add(
        'oldmobile',
        <div className="Form-group">
          <label>原手机号</label>
          <input
            className="FormControl"
            placeholder="请填写完整的原手机号"
            type="number"
            bidi={this.oldmobile}
          />
        </div>,
        60
      )
    items.add(
      'newmobile',
      <div className="Form-group">
        <label>新手机号</label>
        <input
          className="FormControl"
          placeholder="请填写新手机号"
          type="number"
          bidi={this.newmobile}
        />
      </div>,
      60
    )
    items.add(
      'msgcode',
      <div className="Form-group">
        <label>验证码</label>
        <view class="auth-line">
          <input 
            className="FormControl msgcode" 
            placeholder="请填写验证码" 
            type="number" 
            bidi={this.code}>

          </input>
          <button type="button" className="Button Button--primary"  
            onclick={this.sendCode.bind(this)}  disabled={this.sendingSms}>{
            this.liked
              ? '获取验证码'
              : `${this.count} 秒后重发`
          }</button>
        </view>
      </div>,
      60
    )
    items.add(
      'submit',
      <div className="Form-group">
        {Button.component(
          {
            className: 'Button Button--primary',
            type: 'submit',
            loading: this.loading,
          },
          '立即修改'
        )}
      </div>,
      -10
    );  
    return items;
  }
  geTel(tel){
    var reg = /^(\d{3})\d{4}(\d{4})$/;
    return tel.replace(reg, "$1****$2");
  }
  countDown() {
    if (this.count === 1) {
      this.count = 60;
      this.liked = true;
      this.sendingSms = false;
    } else {
      this.count = this.count -1;
      this.liked = false;
      setTimeout(this.countDown.bind(this), 1000);
    }
    m.redraw();
  }
  sendCode(){
    if (!this.liked) {
      return;
    }
    if(this.oldmobile()==''){
        app.alerts.show({ type: 'error' },"请填写原手机号！");
        return ;
    }
    if(!this.isMobile(this.oldmobile())){
        app.alerts.show({ type: 'error' },"原手机号格式错误！");
        return ;
    }
    if(this.newmobile()==''){
      app.alerts.show({ type: 'error' },"请填写手机号！");
      return ;
    }
    if(!this.isMobile(this.newmobile())){
      app.alerts.show({ type: 'error' },"手机号格式错误！");
      return ;
    }
    this.sendingSms = true;
    app.request({
        method: 'POST',
        url: app.forum.attribute('apiUrl') + '/sendcode',
        body: { 
          oldmobile: this.oldmobile(),
          phone: this.newmobile(),
          code: this.code(),
        },
        errorHandler: this.onerror.bind(this),
      })
      .then(() => {
        this.countDown();
      });
  }
  onerror(error) {
    error.alert.content = error.response;
    super.onerror(error);
    this.sendingSms = false;
    console.log('err');
  }
  isMobile(value){
    return /^1[3|4|5|6|7|8|9][0-9]\d{8}$/.test(value);
  }
  onsubmit(e) {
    e.preventDefault();
    this.loading = true;
    app
      .request({
        method: 'POST',
        url: app.forum.attribute('apiUrl') + '/editmobile',
        body: { 
            oldmobile: this.oldmobile(),
            phone: this.newmobile(),
            code: this.code(),
        },
        errorHandler: this.onerror.bind(this),
      })
      .then(() => {
        app.modal.close()
        app.alerts.show({ type: 'success' },"绑定成功");
        m.redraw();
      })
      .catch(() => {})
      .then(this.loaded.bind(this));
  }
}
