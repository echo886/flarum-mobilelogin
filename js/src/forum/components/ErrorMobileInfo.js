import Component from 'flarum/common/Component';

export default class ErrorMobileInfo extends Component {
  oninit(vnode) {
    super.oninit(vnode);
  }
  view() {
    return m("div.container",[
            m("span.alert-danger", "检测到你还未绑定手机号，请先绑定手机号。"),
            m("a.bind[href='/settings']", '立即绑定')
        ])
  }
}
