<?php

/*
 * This file is part of fof/oauth.
 *
 * Copyright (c) 2020 FriendsOfFlarum.
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;

return [
    'up' => function (Builder $schema) {
        if (!$schema->hasColumn('users', 'mobile')) {
            $schema->table('users', function (Blueprint $table) {
                $table->string('mobile',15)->nullable();
            });
        }
        $schema->create('wanecho_sms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('mobile');
            $table->string('code');
            $table->tinyInteger('is_active');
            $table->dateTime('created_at');
        });
    },
    'down' => function (Builder $schema) {
        $schema->dropIfExists('wanecho_sms');
    }
];