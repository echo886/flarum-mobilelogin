<?php
namespace Wanecho\Mobilelogin\Api\Controllers;

use Flarum\Api\Controller\AbstractCreateController;
use Illuminate\Support\Arr;
use Psr\Http\Message\ServerRequestInterface as Request;
use Tobscure\JsonApi\Document;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Laminas\Diactoros\Response\EmptyResponse;
use Gregwar\Captcha\CaptchaBuilder;
use Gregwar\Captcha\PhraseBuilder;
use Illuminate\Contracts\Cache\Repository;

class CaptchaController implements RequestHandlerInterface
{

    /**
     * @var Repository
     */
    protected $cache;

    public function __construct(Repository $cache)
    {
        $this->cache = $cache;
    }
    public function handle(Request $request):Response
    {   
        
        $session =  $request->getAttribute('session');
        $phraseBuilder = new PhraseBuilder(4, '0123456789');
        $builder = new CaptchaBuilder(null,$phraseBuilder);
        $builder->setBackgroundColor(255, 255, 255);//背景白色
        $builder->build();
        $this->cache->put('captcha'.$session->getId(),$builder->getPhrase(),100);
        // header('Content-type: image/jpeg');
        $builder->output();
        return new EmptyResponse();
    }
}