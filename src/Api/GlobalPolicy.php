<?php
namespace Wanecho\Mobilelogin\Api;

use Flarum\Settings\SettingsRepositoryInterface;
use Flarum\User\Access\AbstractPolicy;
use Flarum\User\User;
use Flarum\Discussion\Discussion;

class GlobalPolicy extends AbstractPolicy
{
/**
     * @var SettingsRepositoryInterface
     */
    protected $settings;

    public function __construct(SettingsRepositoryInterface $settings)
    {
        $this->settings = $settings;
    }

    public function startDiscussion(User $actor)
    {
        if(!$actor->mobile){
            return $this->forceDeny();
        }
    }

    /**
     * @param Flarum\User\User $actor
     * @param string $ability
     * @return bool|void
     */
    // public function can(User $actor, string $ability)
    // {
    //     if (in_array($ability, ['startDiscussion'])) {
    //         return $this->deny();
    //     }
    // }
}