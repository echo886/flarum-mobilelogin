<?php

namespace Wanecho\Mobilelogin\Api;

use Flarum\Settings\SettingsRepositoryInterface;
use Flarum\User\Access\AbstractPolicy;
use Flarum\User\User;
use Flarum\Discussion\Discussion;

class PostPolicy extends AbstractPolicy
{
    protected $settings;

    public function __construct(SettingsRepositoryInterface $settings)
    {
        $this->settings = $settings;
    }

    public function reply(User $actor, Discussion $discussion)
    {
        if(!$actor->mobile){
            return $this->deny();
        }
    }

   

}