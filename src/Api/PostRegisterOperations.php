<?php
namespace Wanecho\Mobilelogin\Api;

use Flarum\Settings\SettingsRepositoryInterface;
use Flarum\User\Event\Registered;

class PostRegisterOperations
{
    protected $settings;

    public function __construct(SettingsRepositoryInterface $settings)
    {
        $this->settings = $settings;
    }

    public function handle(Registered $event): void
    {
        $user = $event->user;
        $user->save();
    }
}