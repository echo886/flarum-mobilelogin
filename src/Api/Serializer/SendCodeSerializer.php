<?php

namespace Wanecho\Mobilelogin\Api\Serializer;

use Flarum\Api\Serializer\AbstractSerializer;

class SendCodeSerializer extends AbstractSerializer
{
    protected $type = 'sendcode';

    protected function getDefaultAttributes($model)
    {
        return [
           
        ];
    }
}