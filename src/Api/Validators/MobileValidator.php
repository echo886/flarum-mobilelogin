<?php
namespace  Wanecho\Mobilelogin\Api\Validators;

use Flarum\Foundation\AbstractValidator;

class MobileValidator extends AbstractValidator
{
    /**
     * {@inheritdoc}
     */
    protected $rules = [
        'mobile' => [
            'required',
            'mobile'
        ],
        'msgcode' => [
            'required',
            'msgcode'
        ],
        'captcha' => [
            'required',
        ],
    ];

    /**
     * {@inheritdoc}
     */
    protected function getMessages()
    {

        return [
            'mobile'  => '手机号已注册',
            'msgcode' => '短信验证码错误',
        ];
    }

}