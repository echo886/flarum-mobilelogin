<?php
namespace  Wanecho\Mobilelogin\Api\Validators;

use Flarum\Foundation\AbstractValidator;

class RegSendSmsValidator extends AbstractValidator
{
    /**
     * {@inheritdoc}
     */
    protected $rules = [
        'mobile' => [
            'required',
            'mobile'
        ],
        'captcha' => [
            'required',
            'msgcode'
        ],
    ];

    /**
     * {@inheritdoc}
     */
    protected function getMessages()
    {

        return [
            'mobile'  => '手机号已注册',
            'captcha' => '图形验证码错误',
        ];
    }

}