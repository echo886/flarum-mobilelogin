<?php
namespace Wanecho\Mobilelogin\Api;

use Carbon\Carbon;
use Flarum\Database\AbstractModel;
use Flarum\Discussion\Discussion;
use Flarum\Foundation\EventGeneratorTrait;
use Flarum\Group\Group;
use Flarum\Post\Post;
use Flarum\User\User;
use SychO\ActionLog\Event\Logged;

class WanechoSms extends AbstractModel
{
    use EventGeneratorTrait;
    
    protected $guarded = [];
    
    protected $table = 'wanecho_sms';
}